from flask import Flask, render_template, request, jsonify
import requests
from geopy.geocoders import Nominatim

app = Flask(__name__)
API_KEY = '1d0b5d51843073e6de418c04a7cc7043'
WTF_CSRF_ENABLED = 'True'
SECRET_KEY = 'jelly'

#Results list, will hold list of all json data
results = []

#Homepage with beer search
@app.route('/', methods=['GET', 'POST'])
def index():
    return render_template('index.html')

#Route for random beer
@app.route("/randombeer", methods=['GET', 'POST'])
def randombeer():
	url_random = 'http://api.brewerydb.com/v2/beer/random/?key='+API_KEY+'&withBreweries=Y'
	#turns api request into json data
	r = requests.get(url_random)
	randombeer_info = r.json()
	#clear the list before adding to it, this elimniates redundant data
	results[:] = []
	try:
		#for loop that adds all results to results list
		for item in randombeer_info['data']:
			results.append(item)
	except:
		pass
	#render randombeer.html with results 
	return render_template('randombeer.html', results=results)

#Route for searches 
@app.route("/beer", methods=['GET', 'POST'])
def beer():
	#gets text from button on beer.html
	text = str(request.form['text'])

	#if statement to decide what to query
	button = request.form['type'] #this retrieves which radio button was pressed

	if(button == 'beer'):
		url_beer = 'http://api.brewerydb.com/v2/search/?key='+API_KEY+'&q='+text+'&type=beer'
		#turns api request into json data
		r = requests.get(url_beer)
		beer_info = r.json()
		#clear the list before adding to it, this eliminates redundant data
		results[:] = []
		try:
			#for loop that adds all results to results list
			for item in beer_info['data']:
				results.append(item)
		except:
			pass
		#renders beer.html template with results(list) and error_msg
		return render_template('beer5.html', results=results, button=button)

	if(button == 'brewery'):
		url_beer = 'http://api.brewerydb.com/v2/search/?key='+API_KEY+'&q='+text+'&type=brewery'
		#turns api request into json data
		r = requests.get(url_beer)
		beer_info = r.json()
		#clear the list before adding to it, this eliminates redundant data
		results[:] = []
		try:
			#for loop that adds all results to results list
			for item in beer_info['data']:
				results.append(item)
		except:
			pass
		#renders beer.html template with results(list) and error_msg
		return render_template('beer5.html', results=results, button=button)

	if(button == 'location'):
		geolocator = Nominatim()
		location = geolocator.geocode(text)
		#Error handling for no latitude by text entry
		try:
			lat = location.latitude 
		except:
			location_error = 'Sorry, no breweries in this area.'
			#renders beer.html template with results(list) and error_msg
			return render_template('beer3.html', results=results, button=button, location_error=location_error)			
		longitude = location.longitude 
		url_location_brew_search = 'http://api.brewerydb.com/v2/search/geo/point?lat='+str(lat)+'&lng='+str(longitude)+'&key='+API_KEY
		r = requests.get(url_location_brew_search)
		local_info = r.json()
		results[:] = []
		try:
			#for loop that adds all results to results list
			for item in local_info['data']:
				results.append(item)
				brew_id = item['breweryId']
		except:
			pass
		#renders beer.html template with results(list) and error_msg
		return render_template('beer5.html', results=results, button=button)

#runs the web app
if __name__ == '__main__':
	app.run(debug=True)